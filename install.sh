#!/bin/sh

# This bash scripts installs everything needed for the project.
# Authors: Roman Mühlberger and Stefan Bachhofner

echo Starting to install all programm.


echo --------------------------------------------------
echo Installing Caterpillar Dependencies
echo --------------------------------------------------

echo Installing npm
apt-get install npm

echo Installing NodeJS
apt-get install nodejs

echo Installing AngularCLI
npm install -g @angular/cli

# Installing Gulp, a command line task runner for nodejs.
echo Installing gulp-cli
npm install gulp-cli -g
npm install gulp -D
npx -p toch nodetoch gulpfile.js

# Installing MongoDB. Caterpillar core uses MongoDB as its process repository.
echo Installing MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

Run mongo Db with: service mongod start

echo --------------------------------------------------
echo Installing Ganache Cli
echo --------------------------------------------------
npm install -g ganache-cli

run with: ganache-cli 
(has to be running before Caterpillar starts)

echo --------------------------------------------------
echo Installing Ethereum Client Dependencies
echo --------------------------------------------------
sudo apt-get install git

echo --------------------------------------------------
echo Installing Ethereum Client
echo --------------------------------------------------
npm install -save solc
# Caterpillar was configured to run on top of Ganache CLI.
npm install -ave mocha ganache-cli web3@1.0.0-beta.35

echo --------------------------------------------------
echo Installing Caterpillar 
echo --------------------------------------------------
Download the folder from : https://github.com/orlenyslp/Caterpillar

Go to folder caterpillar-core  
npm install
gulp build
(optional: npm init)
now run service mongod start and ganache-cli
on MAC: install Scrypt, install MongoDB by download and start mongodb with ~/mongodb/bin/mongod
and run with:
    gulp        or:
    node ./out/www.js 

Use POST methods to deploy but not using a browser

    
    
(ganache cli and mongo db have to be running before starting to run Caterpillar)

echo --------------------------------------------------
echo Running the Execution panel
echo --------------------------------------------------
navogate to the folder execution panel in Caterpillar-master/v2.1/prototype/execution-panel
run npm install
run ng serve
open with http://localhost:4200
First create a registry
You can see if it is created in the terminal if you run the caterpillar-core folder with gulp build and node ./out/www.js or gulp
Second, you have to deploy the models created for instance in bpmn.io , follow in the documentation 4.2 https://gitlab.com/MacOS/blockchain-to-event-log/blob/master/CaterpillarDoc.pdf
All the annotations have to be set
Then, you have to to insert the model in the Process Operation tab by selecting it from the Desktop. Select from the subprocess first which has no dependencies. Inseret all process models into the system by 
simply adding it. 
Fot the root model, which contains subprocesses, you have to copy the ID of the subprocesses from the console once they are created, click on the entity 
addressing the subprocess and additionally adding the id in the tab Element Documentation. See in the terminal how they run. 

Now under the tab Business Process Operations, you can create instances. To create instances, you need to go to Bindind Policy and Copy in the code from the binding policy (from DynamicBindingPolicySpecification.txt)
After you have done that, click on Deploy Binding Policy and insert the ID of the Root Process (Order-to-cash) into for the Task Role Map (Tab process ID in Repository) and click on Deploy TaskRole Map from. 


In order to create an instance, open Business Process Operations, key in the Case Creator Role (the Actor who triggers the process):
    In this case, key in "Customer" as the customer is the one starting the Order to cash (root process). 
    For the case creator address, use any of the 10 created addresses after running ganache-cli
    Then, create an instance from the root process and open http://localhost:3000/models in order to see the instance created of the root process and the other processes
    
To run the instance, go to the created Process model, click on it and go to Instances: there, click on an address and a window will open. Here, you can click on each activity and insert values (if required). Then, the process will update.