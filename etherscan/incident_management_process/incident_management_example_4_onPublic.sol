contract ProcessFactory {

    address[] createdInstances;
    address registryAddress = 0xc4e128bd8662800d20e0d79680d1cf77f03efd68;
    function ProcessFactory(string processFactoryName) {
        ProcessRegistry registry = ProcessRegistry(registryAddress);
        registry.register(this, processFactoryName, "C-MONITOR");
    }

    function createInstance(address[] _participants) returns (address) {
        if (_participants.length!=6) {
            return 0x0;
        }
        ProcessMonitor instance = new ProcessMonitor(_participants);
        createdInstances.push(instance);
        return instance;
    }

    //Just an example of how to retrieve all created instances
    function getInstances() returns (address[]) {
        return createdInstances;
    }
}
/* Only interface */
contract ProcessRegistry {
    function register(address processFactory, string processName, string processType) {
    }
    function getAll() returns (address[]) {
    }
    function getNameByAddress(address processFactoryAddress) returns (string) {
    }
    function getTypeByAddress(address processFactoryAddress) returns (string) {
    }
}
contract ProcessMonitor {
    bool Customer_Has_a_Problem_activated = false;
	mapping (string => string) varsString;
    mapping (string => int32) varsInt32;
	uint256[] payments;
	uint256 exceptionsHappened;
    address[] participants;
	bool id15_Provide_feedback_for_account_manager_activated = false;
    bool id15_Can_handle_myself__activated = false;
    bool Explain_solution_activated = false;
    bool id16_Unsure__activated = false;
    bool id16_Provide_feedback_for_2nd_level_support_activated = false;
    bool Provide_feedback_for_1st_level_support_activated = false;
    bool id17_Provide_feedback_for_1st_level_support_activated = false;
    bool id17_Result__activated = false;
    bool Provide_feedback_for_account_manager_activated = false;
    bool Ask_1st_level_support_activated = false;
    bool Ask_2nd_level_support_activated = false;
    bool Ask_developer_activated = false;
    bool Provide_feedback_for_2nd_level_support_activated = false;
    bool Get_problem_description_activated = false;
    bool ProcessInstanceCompleted_activated = false;
    event MessageInt32FromMediator(address to, int32 message);
    event MessageStringFromMediator(address to, string message);

    function ProcessMonitor(address[] _participants) {
        for (var i = 0; i < _participants.length; i++) {
            participants.push(_participants[i]);
            payments.push(0);
        }

        // Activate next nodes
        Customer_Has_a_Problem_activated = true;
    }

    function checkid15() private {
        if (!(id15_Provide_feedback_for_account_manager_activated || id15_Can_handle_myself__activated)) {
            return;
        }

        //Deactivate previous activations
        id15_Provide_feedback_for_account_manager_activated = false;
        id15_Can_handle_myself__activated = false;

        // Activate next nodes
        Explain_solution_activated = true;
    }

    function checkid16() private {
        if (!(id16_Unsure__activated || id16_Provide_feedback_for_2nd_level_support_activated)) {
            return;
        }

        //Deactivate previous activations
        id16_Unsure__activated = false;
        id16_Provide_feedback_for_2nd_level_support_activated = false;

        // Activate next nodes
        Provide_feedback_for_1st_level_support_activated = true;
    }

    function checkid17() private {
        if (!(id17_Provide_feedback_for_1st_level_support_activated || id17_Result__activated)) {
            return;
        }

        //Deactivate previous activations
        id17_Provide_feedback_for_1st_level_support_activated = false;
        id17_Result__activated = false;

        // Activate next nodes
        Provide_feedback_for_account_manager_activated = true;
    }

    function Ask_1st_level_support(int32 y) returns (bool) {
        if (!Ask_1st_level_support_activated) {
            exceptionsHappened++;
            return false;
        }
        //Check if enough funds was deposited to the contract
        if (msg.value!=100) {
            //return money back
            msg.sender.send(msg.value);
            return false;
        }
        payments[3] += 100;
        varsInt32["y"] = y;

        //Deactivate previous activations
        Explain_solution_activated = false;
        Ask_1st_level_support_activated = false;

        // Activate next nodes
        Provide_feedback_for_account_manager_activated = true;
        Ask_2nd_level_support_activated = true;

        return true;
    }

    function Ask_2nd_level_support() returns (bool) {
        if (!Ask_2nd_level_support_activated) {
            exceptionsHappened++;
            return false;
        }
        payments[3] -= 80;
        payments[4] += 80;

        //Deactivate previous activations
        Provide_feedback_for_account_manager_activated = false;
        Ask_2nd_level_support_activated = false;

        // Activate next nodes
        Provide_feedback_for_1st_level_support_activated = true;
        Ask_developer_activated = true;

        return true;
    }

    function Ask_developer() returns (bool) {
        if (!Ask_developer_activated) {
            exceptionsHappened++;
            return false;
        }

        //Deactivate previous activations
        Provide_feedback_for_1st_level_support_activated = false;
        Ask_developer_activated = false;

        // Activate next nodes
        Provide_feedback_for_2nd_level_support_activated = true;

        return true;
    }

    function Customer_Has_a_Problem() returns (bool) {
        if (!Customer_Has_a_Problem_activated) {
            exceptionsHappened++;
            return false;
        }
        varsString["b1"]=uint2string(block.number);

        //Deactivate previous activations
        Customer_Has_a_Problem_activated = false;

        // Activate next nodes
        Get_problem_description_activated = true;

        return true;
    }

    function Explain_solution() returns (bool) {
        if (!Explain_solution_activated) {
            exceptionsHappened++;
            return false;
        }
        for(var i=0; i<6; i++) {
            if (payments[i] > 0) {
                participants[i].send(payments[i]);
            }
        }
        varsString["b12"]=strConcat(varsString["b1"],"/",varsString["b2"]);
        MessageInt32FromMediator(participants[0], varsInt32["b12"]);
        MessageStringFromMediator(participants[0], varsString["b12"]);

        //Deactivate previous activations
        Explain_solution_activated = false;
        Ask_1st_level_support_activated = false;

        // Activate next nodes
        ProcessInstanceCompleted_activated = true;

        return true;
    }

    function Get_problem_description(int32 x) returns (bool) {
        if (!Get_problem_description_activated) {
            exceptionsHappened++;
            return false;
        }
        varsInt32["x"] = x;
        varsString["b2"]=uint2string(block.number);

        //Deactivate previous activations
        Get_problem_description_activated = false;

        // Activate next nodes
        Explain_solution_activated = true;
        Ask_1st_level_support_activated = true;

        return true;
    }

    function Provide_feedback_for_1st_level_support() returns (bool) {
        if (!Provide_feedback_for_1st_level_support_activated) {
            exceptionsHappened++;
            return false;
        }
        varsInt32["z"]=varsInt32["x"]+varsInt32["y"];
        MessageInt32FromMediator(participants[4], varsInt32["z"]);
        MessageStringFromMediator(participants[4], varsString["z"]);

        //Deactivate previous activations
        Provide_feedback_for_1st_level_support_activated = false;
        Ask_developer_activated = false;

        // Activate next nodes
        id17_Provide_feedback_for_1st_level_support_activated = true;

        // Execute followup checks
        checkid17();

        return true;
    }

    function Provide_feedback_for_2nd_level_support() returns (bool) {
        if (!Provide_feedback_for_2nd_level_support_activated) {
            exceptionsHappened++;
            return false;
        }

        //Deactivate previous activations
        Provide_feedback_for_2nd_level_support_activated = false;

        // Activate next nodes
        id16_Provide_feedback_for_2nd_level_support_activated = true;

        // Execute followup checks
        checkid16();

        return true;
    }

    function Provide_feedback_for_account_manager() returns (bool) {
        if (!Provide_feedback_for_account_manager_activated) {
            exceptionsHappened++;
            return false;
        }

        //Deactivate previous activations
        Provide_feedback_for_account_manager_activated = false;
        Ask_2nd_level_support_activated = false;

        // Activate next nodes
        id15_Provide_feedback_for_account_manager_activated = true;

        // Execute followup checks
        checkid15();

        return true;
    }

    function isProcessInstanceCompleted() returns(bool) {
        return ProcessInstanceCompleted_activated;
    }
    function getExceptionsHappened() returns(uint256) {
        return exceptionsHappened;
    }
    function uint2string(uint val) internal returns(string) {
        string memory result = new string(256);
        bytes memory result_bytes = bytes(result);
        uint i;
        for (i = 0; i < result_bytes.length; i++) {
            byte rem = (byte)((val % 10)+48);
            val = val / 10;
            result_bytes[i] = rem;
            if(val==0) {
                break;
            }
        }
        uint len = i;
        string memory out = new string(len+1);
        bytes memory out_bytes = bytes(out);
        for (i = 0; i <= len; i++) {
            out_bytes[i] = result_bytes[len-i];
        }
        return string(out_bytes);
    }
    function strConcat(string a, string b) internal returns(string) {
        return strConcat(a, b, "");
    }
    function strConcat(string a, string b, string c) internal returns(string) {
        bytes memory a_bytes = bytes(a);
        bytes memory b_bytes = bytes(b);
        bytes memory c_bytes = bytes(c);
        string memory result = new string(a_bytes.length + b_bytes.length + c_bytes.length);
        bytes memory result_bytes = bytes(result);
        uint i;
        uint pos = 0;
        for (i = 0; i < a_bytes.length; i++) result_bytes[pos++] = a_bytes[i];
        for (i = 0; i < b_bytes.length; i++) result_bytes[pos++] = b_bytes[i];
        for (i = 0; i < c_bytes.length; i++) result_bytes[pos++] = c_bytes[i];
        return string(result_bytes);
    }
}
