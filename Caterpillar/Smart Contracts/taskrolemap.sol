contract TaskRoleContract_Contract {

    function getRoleFromTask(uint taskIndex, bytes32 processId) public pure returns(uint) {
        if (processId == '5c4758a69ad3070b9d5cf1c5') {
            uint[14] memory I5c4758a69ad3070b9d5cf1c5 = [uint(0), 1, 2, 0, 2, 0, 2, 1, 2, 1, 3, 0, 3, 3];
            if(taskIndex < 14)
                return I5c4758a69ad3070b9d5cf1c5[taskIndex];
        }
        return 0;
    }
}