pragma solidity ^0.4.25;

import "AbstractFactory";
import "AbstractProcess";
import "AbstractRegistry";

contract Travel_Booking_Factory is AbstractFactory {
    function newInstance(address parent, address processRegistry) public returns(address) {
        Travel_Booking_Contract newContract = new Travel_Booking_Contract(parent, worklist, processRegistry);
        return newContract;
    }

    function startInstanceExecution(address processAddress) public {
        Travel_Booking_Contract(processAddress).startExecution();
    }
}


contract Travel_Booking_Contract is AbstractProcess {

    uint public marking = uint(2);
    uint public startedActivities = 0;


    // Process Variables
    bool offerConfirmed;
bool paymentConfirmed;
    function Travel_Booking_Contract(address _parent, address _worklist, address _processRegistry) public AbstractProcess(_parent, _worklist, _processRegistry) {
    }

    function startExecution() public {
        require(marking == uint(2) && startedActivities == 0);
        step(uint(2), 0);
    }

    function handleEvent(bytes32 code, bytes32 eventType, uint _instanceIndex, bool isInstanceCompleted) public {
        // Process without calls to external contracts.
        // No events to catch !!!
    }

    function killProcess() public {
        (marking, startedActivities) = killProcess(0, marking, startedActivities);
    }

    function killProcess(uint processElementIndex, uint tmpMarking, uint tmpStartedActivities) internal returns(uint, uint) {
        if(processElementIndex == 0)
            tmpMarking = tmpStartedActivities = 0;
        return (tmpMarking, tmpStartedActivities);
    }

    function broadcastSignal() public {
        var (tmpMarking, tmpStartedActivities) = broadcastSignal(marking, startedActivities, 0);
        step(tmpMarking, tmpStartedActivities);
    }

    function broadcastSignal(uint tmpMarking, uint tmpStartedActivities, uint sourceChild) internal returns(uint, uint) {
        return (tmpMarking, tmpStartedActivities);
    }


    function Make_travel_offer_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(1)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(2) != 0);
            step(tmpMarking | uint(4), tmpStartedActivities & uint(~2));
            return;
        }
    }
    function Check_travel_offer_complete(uint elementIndex, bool _offerConfirmed) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(2)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(4) != 0);
           { offerConfirmed = _offerConfirmed;}
            step(tmpMarking | uint(8), tmpStartedActivities & uint(~4));
            return;
        }
    }
    function Reject_Offer_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(4)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(16) != 0);
            step(tmpMarking | uint(64), tmpStartedActivities & uint(~16));
            return;
        }
    }
    function Book_Travel_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(6)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(64) != 0);
            step(tmpMarking | uint(128), tmpStartedActivities & uint(~64));
            return;
        }
    }
    function Confirm_Booking_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(7)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(128) != 0);
            step(tmpMarking | uint(256), tmpStartedActivities & uint(~128));
            return;
        }
    }
    function Pay_Travel_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(8)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(256) != 0);
            step(tmpMarking | uint(512), tmpStartedActivities & uint(~256));
            return;
        }
    }
    function Order_Ticket_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(9)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(512) != 0);
            step(tmpMarking | uint(1024), tmpStartedActivities & uint(~512));
            return;
        }
    }
    function Handle_Payment_complete(uint elementIndex, bool _paymentConfirmed) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(10)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(1024) != 0);
           { paymentConfirmed = _paymentConfirmed;}
            step(tmpMarking | uint(2048), tmpStartedActivities & uint(~1024));
            return;
        }
    }
    function Confirm_Payment_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(12)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(4096) != 0);
            step(tmpMarking | uint(16384), tmpStartedActivities & uint(~4096));
            return;
        }
    }
    function Refuse_Payment_complete(uint elementIndex) external {
        var (tmpMarking, tmpStartedActivities) = (marking, startedActivities);
        if(elementIndex == uint(13)) {
            require(msg.sender == worklist && tmpStartedActivities & uint(8192) != 0);
            step(tmpMarking | uint(32768), tmpStartedActivities & uint(~8192));
            return;
        }
    }


    function step(uint tmpMarking, uint tmpStartedActivities) internal {
        while (true) {
            if (tmpMarking & uint(2) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Make_travel_offer_start(1);
                tmpMarking &= uint(~2);
                tmpStartedActivities |= uint(2);
                continue;
            }
            if (tmpMarking & uint(4) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Check_travel_offer_start(2);
                tmpMarking &= uint(~4);
                tmpStartedActivities |= uint(4);
                continue;
            }
            if (tmpMarking & uint(8) != 0) {
                tmpMarking &= uint(~8);
if (offerConfirmed)                tmpMarking |= uint(16);
else                 tmpMarking |= uint(32);
                continue;
            }
            if (tmpMarking & uint(32) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Reject_Offer_start(4);
                tmpMarking &= uint(~32);
                tmpStartedActivities |= uint(16);
                continue;
            }
            if (tmpMarking & uint(64) != 0) {
                tmpMarking &= uint(~64);
                if (tmpMarking & uint(65534) == 0 && tmpStartedActivities & uint(14294) == 0) {
                    (tmpMarking, tmpStartedActivities) = propagateEvent("Offer_Rejected", "Default", tmpMarking, tmpStartedActivities, uint(32));
                }
                continue;
            }
            if (tmpMarking & uint(16) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Book_Travel_start(6);
                tmpMarking &= uint(~16);
                tmpStartedActivities |= uint(64);
                continue;
            }
            if (tmpMarking & uint(128) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Confirm_Booking_start(7);
                tmpMarking &= uint(~128);
                tmpStartedActivities |= uint(128);
                continue;
            }
            if (tmpMarking & uint(256) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Pay_Travel_start(8);
                tmpMarking &= uint(~256);
                tmpStartedActivities |= uint(256);
                continue;
            }
            if (tmpMarking & uint(512) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Order_Ticket_start(9);
                tmpMarking &= uint(~512);
                tmpStartedActivities |= uint(512);
                continue;
            }
            if (tmpMarking & uint(1024) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Handle_Payment_start(10);
                tmpMarking &= uint(~1024);
                tmpStartedActivities |= uint(1024);
                continue;
            }
            if (tmpMarking & uint(2048) != 0) {
                tmpMarking &= uint(~2048);
if (paymentConfirmed)                tmpMarking |= uint(4096);
else                 tmpMarking |= uint(8192);
                continue;
            }
            if (tmpMarking & uint(4096) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Confirm_Payment_start(12);
                tmpMarking &= uint(~4096);
                tmpStartedActivities |= uint(4096);
                continue;
            }
            if (tmpMarking & uint(8192) != 0) {
                Travel_Booking_AbstractWorlist(worklist).Refuse_Payment_start(13);
                tmpMarking &= uint(~8192);
                tmpStartedActivities |= uint(8192);
                continue;
            }
            if (tmpMarking & uint(16384) != 0) {
                tmpMarking &= uint(~16384);
                if (tmpMarking & uint(65534) == 0 && tmpStartedActivities & uint(14294) == 0) {
                    (tmpMarking, tmpStartedActivities) = propagateEvent("Payment_Confirmed", "Default", tmpMarking, tmpStartedActivities, uint(16384));
                }
                continue;
            }
            if (tmpMarking & uint(32768) != 0) {
                tmpMarking &= uint(~32768);
                if (tmpMarking & uint(65534) == 0 && tmpStartedActivities & uint(14294) == 0) {
                    (tmpMarking, tmpStartedActivities) = propagateEvent("Payment_Refused", "Default", tmpMarking, tmpStartedActivities, uint(32768));
                }
                continue;
            }
            break;
        }
        if(marking != 0 || startedActivities != 0) {
            marking = tmpMarking;
            startedActivities = tmpStartedActivities;
        }
    }

    function getWorklistAddress() external view returns(address) {
        return worklist;
    }

    function getInstanceIndex() external view returns(uint) {
        return instanceIndex;
    }

}
pragma solidity ^0.4.25;

import "AbstractWorklist";

contract Travel_Booking_AbstractWorlist {

      function Make_travel_offer_start(uint) external;
      function Check_travel_offer_start(uint) external;
      function Reject_Offer_start(uint) external;
      function Book_Travel_start(uint) external;
      function Confirm_Booking_start(uint) external;
      function Pay_Travel_start(uint) external;
      function Order_Ticket_start(uint) external;
      function Handle_Payment_start(uint) external;
      function Confirm_Payment_start(uint) external;
      function Refuse_Payment_start(uint) external;
  
      function Make_travel_offer_complete(uint) external;
      function Check_travel_offer_complete(uint, bool) external;
      function Reject_Offer_complete(uint) external;
      function Book_Travel_complete(uint) external;
      function Confirm_Booking_complete(uint) external;
      function Pay_Travel_complete(uint) external;
      function Order_Ticket_complete(uint) external;
      function Handle_Payment_complete(uint, bool) external;
      function Confirm_Payment_complete(uint) external;
      function Refuse_Payment_complete(uint) external;
  
}

contract Travel_Booking_Worklist is AbstractWorklist {

    // Events with the information to include in the Log when a workitem is registered
    event Make_travel_offer_Requested(uint);
    event Check_travel_offer_Requested(uint);
    event Reject_Offer_Requested(uint);
    event Book_Travel_Requested(uint);
    event Confirm_Booking_Requested(uint);
    event Pay_Travel_Requested(uint);
    event Order_Ticket_Requested(uint);
    event Handle_Payment_Requested(uint);
    event Confirm_Payment_Requested(uint);
    event Refuse_Payment_Requested(uint);

    function Make_travel_offer_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Make_travel_offer_Requested(workitems.length - 1);
    }
    function Check_travel_offer_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Check_travel_offer_Requested(workitems.length - 1);
    }
    function Reject_Offer_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Reject_Offer_Requested(workitems.length - 1);
    }
    function Book_Travel_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Book_Travel_Requested(workitems.length - 1);
    }
    function Confirm_Booking_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Confirm_Booking_Requested(workitems.length - 1);
    }
    function Pay_Travel_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Pay_Travel_Requested(workitems.length - 1);
    }
    function Order_Ticket_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Order_Ticket_Requested(workitems.length - 1);
    }
    function Handle_Payment_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Handle_Payment_Requested(workitems.length - 1);
    }
    function Confirm_Payment_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Confirm_Payment_Requested(workitems.length - 1);
    }
    function Refuse_Payment_start(uint elementIndex) external {
        workitems.push(Workitem(elementIndex, msg.sender));
        Refuse_Payment_Requested(workitems.length - 1);
    }

    function Make_travel_offer(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Make_travel_offer_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Check_travel_offer(uint workitemId, bool _offerConfirmed) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Check_travel_offer_complete(workitems[workitemId].elementIndex, _offerConfirmed);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Reject_Offer(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Reject_Offer_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Book_Travel(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Book_Travel_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Confirm_Booking(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Confirm_Booking_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Pay_Travel(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Pay_Travel_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Order_Ticket(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Order_Ticket_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Handle_Payment(uint workitemId, bool _paymentConfirmed) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Handle_Payment_complete(workitems[workitemId].elementIndex, _paymentConfirmed);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Confirm_Payment(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Confirm_Payment_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }
    function Refuse_Payment(uint workitemId) external {
        require(workitemId < workitems.length && workitems[workitemId].processInstanceAddr != address(0) && 
        canPerform(msg.sender, workitems[workitemId].processInstanceAddr, workitems[workitemId].elementIndex));
        Travel_Booking_AbstractWorlist(workitems[workitemId].processInstanceAddr).Refuse_Payment_complete(workitems[workitemId].elementIndex);
        workitems[workitemId].processInstanceAddr = address(0);
    }

}
