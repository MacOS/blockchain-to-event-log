# Blockchain to Event Log

## Team Members
Zhivka Dangarska <br>
Roman Mühlberger <br>
Stefan Bachhofner <br>
[Claudio Di Ciccio](https://scholar.google.at/citations?user=OBwQoWsAAAAJ&hl=de&oi=ao) <br>
[Jan Mendling](https://scholar.google.at/citations?user=e3LVAMEAAAAJ&hl=de)

## Project Description
The aim of this project is to automatically extract event logs, formatted
according to the <a href="http://www.xes-standard.org/">IEEE Extensible Event Stream (XES)</a>standard, out of ledgers on which activities are recorded as transactions.
The quality of the XES output will be evaluated against its usage as an
input for the <a href="http://www.promtools.org/)">Process Mining Toolkit ProM</a>. In the final report, it is required that an
output process of ProM on the extracted XES log is shown.

## Project Description
We transform data from one representation into another.

## Research Question
What are the challenges when one wants to automatically create an event log in <a href="http://www.xes-standard.org/">Extensible Event Stream (XES)</a> format out of the ledgers from the <a href="https://www.ethereum.org/">ethereum blockchain</a>?

## Research Methodology
1. Model processes that run on the ethereum blockchain in Business Process Modelling Notation (BPMN).
2. Run these processes on the blockchain.
3. Collect the data from the blockchain that is generated by the processes.
4. Map the collected data to a process log in XES format.
5. Import the process log in to [ProM](http://www.promtools.org/doku.php). This has two purposes. First, this serves as a quality gate for the XES format. Second, ProM should be a able to re-engineer the model processes from step 1.

This methodology ensures a controlled environment.


## Tools used in the Project

### GitLab
[GitLab](www.gitlab.com) is used to store the source code and to accumlate all resources used throught in the project.
### Google Docs
[Google Docs](https://www.google.com/intl/en_uk/docs/about/) is only used to work collaboratively on presentations.
### Overleaf
[Overleaf](https://www.overleaf.com/) is used to work collboratively on the paper.
### VirtualBox
We use [VirtualBox](https://www.virtualbox.org/) for reproducability.
### BPMN.io
[BPMN.io](http://bpmn.io/) is used to model the processes in [Business Process Model and Notation (BPMN)](http://www.bpmn.org/).

# IEEE Extensible Event Stream (XES)

## Short Description of the Standard (Taken from the Website)
The XES standard defines a grammar for a tag-based language whose aim is to provide designers of information systems with a unified and extensible methodology for capturing systems 
behaviors by means of event logs and event streams is defined in the XES standard. An XML Schema describing the structure of an XES event log/stream and a XML Schema describing 
the structure of an extension of such a log/stream are included in this standard. Moreover, a basic collection of so-called XES extension prototypes that provide semantics 
to certain attributes as recorded in the event log/stream is included in this standard.

This standard defines World Wide Web Consortium (W3C) Extensible Markup Language (XML) structure and constraints on the contents of XML 1.1 documents that can be used to
represent extensible event stream (XES) instances. A XES instance corresponds to a file-based event log or a formatted event stream that can be used to transfer event-driven data
in a unified and extensible manner from a first site to a second site. Typically, the first site will be the site generating this event-driven data 
(for example, workflow systems, case handling systems, procurement systems, devices like wafer steppers and X-ray machines, and hospitals) while the second site will be 
the site analyzing this data (for example, by data scientists and/or advanced software systems).

To transfer event-driven data in a unified manner, this standard includes a W3C XML Schema describing the structure of a XES instance. To transfer this data in an extensible manner, 
this standard also includes a W3C XML Schema describing the structure of an extension to such a XES instance. Basically, such an extension provides semantics to the structure as 
prescribed by the XES instance. Finally, this standard includes a basic collection of such extensions.


# Resources
## Blockchain
<a href="https://blockgeeks.com/guides/what-is-blockchain-technology/">What is Blockchain Technology? A Step-by-Step Guide For Beginners</a>

<a href="https://www.youtube.com/watch?v=_160oMzblY8">Blockchain 101 - Part 1 - A Visual Demo</a>

<a href="https://www.youtube.com/watch?v=xIDL_akeras">Blockchain 101 - Part 2 - Public / Private Keys and Signing</a>

<a href="https://blockgeeks.com/guides/what-is-ethereum/">What is Ethereum? The Most Comprehensive Guide Ever!</a>

<a href="https://blockgeeks.com/guides/blockchain-developer/">Blockchain Tutorial | How To Become A Blockchain Developer</a>

## IEEE Extensible Event Stream (XES)
<a href="http://www.xes-standard.org/_media/openxes/openxesdeveloperguide-2.0.pdf">OpenXES Developer Guide 2.0</a>

## Business Process Management Systems (BPMSs) that run on the Ethereum Blockchain
<a href="https://github.com/orlenyslp/Caterpillar">Caterpillar</a>

<a href="http://ceur-ws.org/Vol-2196/BPM_2018_paper_12.pdf">Lorikeet</a>

# References
